<!doctype html>
<!--[if lt IE 9]> <html class="ie" lang="en"> <![endif]-->
<!--[if gte IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>klau &mdash; Design &amp; Web Development</title>
	<!-- meta -->
	<meta charset="utf-8">
	<meta name="description" content="Web developer based in Auckland, New Zealand with 3 years+ experience and solid understanding of web development. As a front-end developer I am constantly seeking to develop myself by learning new technologies.">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript,Designer,New Zealand,Front-End Developer,Web Design,Auckland,Hamilton,jQuery,Responsive">
  <meta property="og:image" content="http://klau.nz/screenshot.jpg" />
  <link rel="image_src" href="http://klau.nz/screenshot.jpg" / >
  <!-- favicon -->
  <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <link rel="shortcut icon" href="favicon.png">
	<!-- css -->
	<link rel="stylesheet" href="css/style.css">
	<!-- js -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<script src="js/jquery-2.1.0.js"></script>
	<!-- iOS -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<script type="text/javascript">
		$(document).ready(function() {
      // Picture element HTML5 shiv
      document.createElement( "picture" );
		});
	</script>
  <script src="js/picturefill.min.js"></script>
</head>
<body>
  <!-- start video -->
	<video autoplay muted loop poster="/images/hero.jpg" class="bgvid" >
	 <source src="images/video.mp4" type="video/mp4">
	</video>
  <!-- start loader -->
  <div class="custom-preloader">
    <div class="preloader">
      <div class="spinner">
        <div class="bounce1"> </div>
        <div class="bounce2"> </div>
        <div class="bounce3"> </div>
      </div><!-- end .spinner -->
    </div><!-- end .preloader -->
  </div><!-- end .custom-preloader -->
	<nav>
		<div class="center">
      <!-- #logo -->
			<a href="/" id="logo" class="logo ir" title="Klau">Klau</a>
			<div class="controller clearfix">
				<div class="nav-holder">
					<ul class="nav">
            <li><a href="#products">Icons</a></li>
						<li><a href="#work">Projects</a></li>
						<li><a href="#contact">Contact</a></li>
					</ul>
				</div><!-- end .nav-holder -->
        <a href="https://www.iconfinder.com/klauco?ref=klauco" target="_blank" class="ir right iconfinder">Iconfinder</a>
				<a href="https://www.linkedin.com/in/klaunz" target="_blank" class="ir right linkedin">LinkedIn</a>
			</div><!-- end .controller -->
		</div><!-- end .center -->
	</nav>
	<div class="page fullpage">
		<div class="section section0" id="section0">
      <div class="intro-border">
  			<div class="intro">
          <h3>Think<br>Outside</h3> <!-- class="blue" -->
  				<h3 class="size-2">The Box</h3>
  			</div><!-- end .intro -->
      </div><!-- end .intro-border -->
			<span class="overlay"></span>
      <a href="#bio" class="arrow bounce"></a>
		</div><!-- end #section0 -->
	</div><!-- end #fullpage -->

  <div id="bio" class="page bio page-row">
    <div class="center a-center">
      <div class="revealOnScroll" data-animation="fadeInDown">
        <p><span class="join">A passionate web developer and designer</span></p>
      </div><!-- .revealOnScroll -->
      <div class="skills">
        <div class="revealOnScroll" data-timeout="100"  data-animation="flipInX">
          Web&nbsp;Development
        </div><!-- end .revealOnScroll -->
        <div class="revealOnScroll" data-timeout="300"  data-animation="flipInX">
          UI/UX&nbsp;Design
        </div><!-- end .revealOnScroll -->
        <div class="revealOnScroll" data-timeout="500"  data-animation="flipInX">
          Icon&nbsp;Design
        </div><!-- end .revealOnScroll -->
        <div class="revealOnScroll" data-timeout="700"  data-animation="flipInX">
          Graphic&nbsp;Design
        </div><!-- end .revealOnScroll -->
      </div><!-- end .skills -->
    </div><!-- end .center -->
    <div class="full-img"></div>
  </div><!-- end #bio -->

  <div id="products" class="page products page-row">
    <div class="center a-center">
      <div class="intro">
        <h2>Icon Design</h2>
        <p>Find my latest collections here or follow me on <a href="https://www.iconfinder.com/klauco?ref=klauco" target="_blank">Iconfinder</a>.</p>
      </div><!-- end .intro -->
      <div class="sets">
        <div class="collection">
          <h3 class="alt">Beryllium Web Set</h3>
          <img src="images/collections/emerald-gradient-web-set.png" alt="Emerald Gradient Web Set">
          <p><a class="button" target="_blank" href="https://www.iconfinder.com/iconsets/emerald-gradient-web-set?ref=klauco">Preview</a></p>
        </div><!-- end .collection -->
      </div><!-- end .sets -->
    </div><!-- end .center -->
  </div><!-- end #products -->

	<div id="work" class="page work page-row grey-bg">
		<div class="center">
			<h2>Featured Projects</h2>
			<ul class="chocolat-parent">
        <li class="revealOnScroll" data-timeout="200"  data-animation="zoomIn">
          <div class="wrapper">
            <img src="images/portfolio/thumb_thehorse.jpg" alt="The Horse">
            <div class="overlay">
              <span class="textleft">The Horse</span>
              <span class="textright">The Horse</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="https://www.thehorse.com.au/" href="images/portfolio/thehorse-640x2622.jpg" data-caption="<a target='_blank' href='https://www.thehorse.com.au/'>The Horse <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="https://www.thehorse.com.au/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
        </li>
        <li class="revealOnScroll" data-timeout="200"  data-animation="zoomIn">
          <div class="wrapper">
            <img src="images/portfolio/thumb_ethique.jpg" alt="Ethique">
            <div class="overlay">
              <span class="textleft">Ethique Beauty</span>
              <span class="textright">Ethique Beauty</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="https://ethiqueworld.com/" href="images/portfolio/ethique-640x2016.jpg" data-caption="<a target='_blank' href='https://ethiqueworld.com/'>Ethique Beauty <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="https://ethiqueworld.com/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
        </li>
        <li class="revealOnScroll" data-timeout="200"  data-animation="zoomIn">
          <div class="wrapper">
            <img src="images/portfolio/thumb_95bfm.jpg" alt="95bfm">
            <div class="overlay">
              <span class="textleft">95BFM</span>
              <span class="textright">95BFM</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://www.95bfm.co.nz/" href="images/portfolio/95bfm-640x997.jpg" data-caption="<a target='_blank' href='http://www.95bfm.co.nz/'>95BFM <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://www.95bfm.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
        </li>
        <li class="revealOnScroll" data-timeout="400"  data-animation="zoomIn">
          <div class="wrapper">
            <img src="images/portfolio/thumb_subaru.jpg" alt="Subaru">
            <div class="overlay">
              <span class="textleft">Subaru</span>
              <span class="textright">Subaru</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="https://www.subaru.co.nz/" href="images/portfolio/subaru-640x1939.jpg" data-caption="<a target='_blank' href='https://www.subaru.co.nz/'>Subaru <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="https://www.subaru.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
        </li>
        <li class="revealOnScroll" data-timeout="400"  data-animation="zoomIn">
          <div class="wrapper">
            <img src="images/portfolio/thumb_cleverbrewing.jpg" alt="Clever Brewing">
            <div class="overlay">
              <span class="textleft">Clever Brewing</span>
              <span class="textright">Clever Brewing</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://cleverbrewing.com.au" href="images/portfolio/cleverbrewing-01-640x1843.jpg" data-caption="<a target='_blank' href='http://cleverbrewing.com.au'>Clever Brewing <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://cleverbrewing.com.au"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
        </li>
				<li class="revealOnScroll" data-timeout="200"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_virscient.jpg" alt="Virscient">
            <div class="overlay">
							<span class="textleft">Virscient</span>
							<span class="textright">Virscient</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://www.virscient.com/" href="images/portfolio/virscient-01-640x974.jpg" data-caption="<a target='_blank' href='http://www.virscient.com/'>Virscient <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://www.virscient.com/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
				<li class="revealOnScroll" data-timeout="400"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_herdofcows.jpg" alt="Herd Of Cows">
            <div class="overlay">
							<span class="textleft">Herd of Cows</span>
							<span class="textright">Herd of Cows</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://herdofcowsmorrinsville.co.nz/" href="images/portfolio/herdofcows-01-640x976.jpg" data-caption="<a target='_blank' href='http://herdofcowsmorrinsville.co.nz/'>Herd Of Cows <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://herdofcowsmorrinsville.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
				<li class="revealOnScroll" data-timeout="600"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_mdictionary.jpg" alt="Māori Dictionary">
            <div class="overlay">
							<span class="textleft">Maori Dictionary</span>
							<span class="textright">Maori Dictionary</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://maoridictionary.co.nz/" href="images/portfolio/maoridictionary-01-680x1398.jpg" data-caption="<a target='_blank' href='http://maoridictionary.co.nz/'>Maori Dictionary <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://maoridictionary.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
				<li class="revealOnScroll" data-timeout="200"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_ozone.jpg" alt="Ozone">
            <div class="overlay">
							<span class="textleft">Ozone</span>
							<span class="textright">Ozone</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://ozonekites.com/products/water-kites/enduro-v1/" href="images/portfolio/ozone-01-640x964.jpg" data-caption="<a target='_blank' href='http://ozonekites.com/products/water-kites/enduro-v1/'>Ozone <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://ozonekites.com/products/water-kites/enduro-v1/" href="images/portfolio/ozone-01-640x964.jpg"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
				<li class="revealOnScroll" data-timeout="600"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_msjd.jpg" alt="Ms. JD">
            <div class="overlay">
							<span class="textleft">Ms. JD</span>
							<span class="textright">Ms. JD</span>
                <div class="control">
                  <a class="ico-view ir chocolat-image" title="View Project" data-location="http://ms-jd.org/" href="images/portfolio/msjd-01-640x1327.jpg" data-caption="<a target='_blank' href='http://ms-jd.org/'>Ms.JD <span>Visit website</span></a>"></a>
                  <a class="ir ico-website" title="Visit Website" target="_blank" href="http://ms-jd.org/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
				<li class="revealOnScroll" data-timeout="200"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_lunchorders.jpg" alt="Lunch Orders">
            <div class="overlay">
							<span class="textleft">Lunch Orders</span>
							<span class="textright">Lunch Orders</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://lunchorders.co.nz/" href="images/portfolio/lunchorders-01-640x735.jpg" data-caption="<a target='_blank' href='http://lunchorders.co.nz/'>Lunch Orders <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://lunchorders.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
				<li class="revealOnScroll" data-timeout="400"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_mytaxback.jpg" alt="My Tax Back">
            <div class="overlay">
							<span class="textleft">My Tax Back</span>
							<span class="textright">My Tax Back</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="https://mytaxback.co.nz/" href="images/portfolio/mytaxback-640x994.jpg" data-caption="<a target='_blank' href='https://mytaxback.co.nz/'>My Tax Back <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="https://mytaxback.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
        <li class="revealOnScroll" data-timeout="600"  data-animation="zoomIn">
          <div class="wrapper">
            <img src="images/portfolio/thumb_aruba.jpg" alt="">
            <div class="overlay">
              <span class="textleft">Aruba Top Drive</span>
              <span class="textright">Aruba Top Drive</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="https://www.arubatopdrive.com/" href="images/portfolio/aruba-640x1185.jpg" data-caption="<a target='_blank' href='https://www.arubatopdrive.com/'>Aruba Top Drive <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="https://www.arubatopdrive.com/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
        </li>
        <li class="revealOnScroll" data-timeout="200"  data-animation="zoomIn">
          <div class="wrapper">
            <img src="images/portfolio/thumb_homeandgarden.jpg" alt="">
            <div class="overlay">
              <span class="textleft">Home &amp; Garden Show</span>
              <span class="textright">Home &amp; Garden Show</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="https://www.homeandgardenshow.co.nz/" href="images/portfolio/homeandgarden-640x1330.jpg" data-caption="<a target='_blank' href='https://www.homeandgardenshow.co.nz/'>Home &amp; Garden Show <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="https://www.homeandgardenshow.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
        </li>
				<li class="revealOnScroll" data-timeout="400"  data-animation="zoomIn">
          <div class="wrapper">
						<img src="images/portfolio/thumb_hamiltongardens.jpg" alt="">
            <div class="overlay">
							<span class="textleft">Hamilton Gardens</span>
							<span class="textright">Hamilton Gardens</span>
              <div class="control">
                <a class="ico-view ir chocolat-image" title="View Project" data-location="http://hamiltongardens.co.nz/" href="images/portfolio/hamilton-gardens-01-640x564.jpg" data-caption="<a target='_blank' href='http://hamiltongardens.co.nz/'>Hamilton Gardens <span>Visit website</span></a>"></a>
                <a class="ir ico-website" title="Visit Website" target="_blank" href="http://hamiltongardens.co.nz/"></a>
              </div><!-- end .control -->
            </div><!-- end .overlay -->
          </div><!-- end .wrapper -->
				</li>
			</ul>
		</div><!-- end .center -->
	</div><!-- end #work -->

	<div id="contact" class="contact page page-row">
		<div class="center">
			<h2>Contact</h2>	  
			<form id="ajax-contact" method="post" class="validate" action="mailer.php">
				<div class="form-messages"></div>
				<ul>
					<li class="fields">
						<fieldset class="left half">
							<input type="text" id="name" class="name required" name="name" placeholder="Name">
						</fieldset>
						<fieldset class="left half">
							<input type="email" name="email" class="email required" id="email" placeholder="Email">
						</fieldset>
					</li>
					<li class="fields">
						<fieldset>
							<textarea id="message" placeholder="Message" class="required" name="message"></textarea>
						</fieldset>
					</li>
				</ul>
				<button type="submit">Submit</button>
			</form>
		</div><!-- end .center -->
	</div><!-- end #contact -->

	<footer class="footer">
    <div class="follow-us">
      <a href="https://www.iconfinder.com/klauco?ref=klauco" target="_blank" class="ir right iconfinder">Iconfinder</a>
      <a href="https://www.linkedin.com/in/klaunz" target="_blank" class="ir right linkedin">LinkedIn</a>
    </div><!-- end .follow-us -->
    <!-- facebook widget -->
		<div class="fb-like" data-href="http://klau.nz/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<br>
    <!-- copyright -->
		<p>&copy; <?php echo date("Y"); ?> Copyright klau. All rights reserved.</p>
	</footer>

  <!-- javaScript -->
	<script src="js/selectivizr-min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/jquery.mask.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/jquery.chocolat.min.js"></script>
	<script src="js/init.js"></script>	
</body>
</html>
