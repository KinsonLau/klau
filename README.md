# Custom template for klau
This project was built using a custom template created and modified by Copyright (c) Kinson Lau

URL - http://klau.nz/

Language - PHP

## Overview

Base project folder for a PHP installation. Requires additional modules to function:

- Yarn package

Yarn caches every package it downloads so it never needs to download it again. It also parallelizes operations to maximize resource utilization so install times are faster than ever.
https://yarnpkg.com/en/

- Sprite Sass

As web developers have become more concerned about browser performance a technique called “image spriting” has emerged that is designed to reduce the number of requests made to the server. As it turns out, fewer requests made the server (when there is no significant difference in the combined size of the files delivered) can make a big difference in how fast a page appears to download.

## Managing site built with Composer

- Bower install

Bower can manage components that contain HTML, CSS, JavaScript, fonts or even image files. Bower doesn’t concatenate or minify code or do anything else - it just installs the right versions of the packages you need and their dependencies.
https://bower.io/

- NodeJs

Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world.
https://nodejs.org/en/

## Contact details

- Email: kinson_11@hotmail.com