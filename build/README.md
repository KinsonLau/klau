# Build

This folder acts as a 'sources' directory for [automatically creating svg
sprites using gulp and Sass](https://www.liquidlight.co.uk/blog/article/creating-svg-sprites-using-gulp-and-sass/).

To upload new svgs add them to the `/build/sprites/` directory then run `npm run gulp` at the theme root.

Be aware to:

Make sure your new file permissions allow everyone to read and write.
Make sure your new svg is sized in round pixels.


